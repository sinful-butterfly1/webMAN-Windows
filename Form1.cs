﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;



namespace Webman_Windows_Frontend
{
    public partial class Form1 : Form
    {
        HtmlAgilityPack.HtmlWeb web = new HtmlAgilityPack.HtmlWeb();
        public string consoleIp = String.Empty;
        public bool connected = false;

        public Form1()
        {
            InitializeComponent();
        }
        public string getTemps(string ip)
        {
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://" + ip + "/cpursx_ps3");
            var tagToLookFor = doc.DocumentNode.SelectSingleNode("//a");
            return tagToLookFor.InnerText;
        }

        public string getPSID(string ip)
        {
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://" + ip + "/cpursx.ps3?/sman.ps3");
            var tagToLookFor = doc.DocumentNode.SelectSingleNode("//span[@id='ht']").InnerText;
            string[] vals = tagToLookFor.Split(' ');
            return vals[3].Substring(0, vals[3].Length - 4);

            
        }

        public string getIDPS(string ip)
        {
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://" + ip + "/cpursx.ps3?/sman.ps3");
            var tagToLookFor = doc.DocumentNode.SelectSingleNode("//span[@id='ht']").InnerText;
            string[] vals = tagToLookFor.Split(' ');
            return vals[8].Substring(0, vals[8].Length - 3  ); ;
        }

        public string getMac(string ip)
        {
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://" + ip + "/cpursx.ps3?/sman.ps3");
            var tagToLookFor = doc.DocumentNode.SelectSingleNode("//span[@id='ht']").InnerText;
            string[] vals = tagToLookFor.Split(' ');
            return vals[11];
        }

        public string getFirmware(string ip)
        {
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://" + ip + "/cpursx.ps3?/sman.ps3");
            var tagToLookFor = doc.DocumentNode.SelectSingleNode("//a[@href='/setup.ps3']").InnerText;
            string[] vals = tagToLookFor.Split(' ');
            string finalStr = vals[0] + " " + vals[1] + " " + vals[2] + " " + vals[3] + " ";
            return finalStr;
        }

        
        public bool consoleCommand(string command)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(command);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse());
                return true;
            }
            catch (WebException e)
            {
                return false;
            } // I know it's ugly
        }
        public bool testConnection(string ip)
        {
            if (consoleCommand("http://" + ip + "/notify.ps3mapi?msg=Welcome+to+the+Windows+frontend+for+webMAN+Mod%21"))
            {
                connected = true;
                consoleIp = ip;
                textBox1.Enabled = false;
                button5.Enabled = false;
                button5.Text = "Connected!";
                return true;
            }
            else
            {
                return false;
            }
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button2.Enabled = false;
            button6.Enabled = false;
            trackBar1.Enabled = false;
            textBox2.Enabled = false;
            button7.Enabled = false;
            button8.Enabled = false;
            comboBox2.Enabled = false;
            button4.Enabled = false;
            button3.Enabled = false;
            label4.Text = "Not connected";
            label5.Text = "Not connected";


        }

        private void button1_Click(object sender, EventArgs e)
        {
            consoleCommand("http://" + consoleIp + "/restart.ps3");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            consoleIp = textBox1.Text;
            if (testConnection(consoleIp))
            {
                MessageBox.Show("Connected!", "Welcome to the Windows frontend for webMAN mod!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                button1.Enabled = true;
                button2.Enabled = true;
                button6.Enabled = true;
                trackBar1.Enabled = true;
                textBox2.Enabled = true;
                button7.Enabled = true;
                button8.Enabled = true;
                button4.Enabled = true;
                comboBox2.Enabled = true;
                button3.Enabled = true;
                label1.Text = "TEMPS: " + getTemps(consoleIp);
                label4.Text = "PSID LV2: " + getPSID(consoleIp);
                label5.Text = "IDPS LV2: "  + getIDPS(consoleIp);
                label7.Text += " " + consoleIp;
                label8.Text = "Mac Address: " + getMac(consoleIp);
                label6.Text = getFirmware(consoleIp);




            }
            else
            {
                MessageBox.Show("Didn't work.", "Something went wrong. Check your IP and try again.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            consoleCommand("http://" + consoleIp + "/shutdown.ps3");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            consoleCommand("http://" + consoleIp + "/insert.ps3");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            consoleCommand("http://" + consoleIp + "/play.ps3");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            label1.Text = "TEMPS: " + getTemps(consoleIp);
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            string fanSpeed = trackBar1.Value.ToString();
            consoleCommand("http://" + consoleIp + "/cpursx.ps3?fan=" + fanSpeed);
            label2.Text = "Current fan speed: " + fanSpeed + "%";

        }

        private void button8_Click(object sender, EventArgs e)
        {
            string textToSend = textBox2.Text;
            consoleCommand("http://" + consoleIp + "/notify.ps3mapi?msg=" + textToSend);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            consoleCommand("http://" + consoleIp + "/refresh.ps3?xmb;/sman.ps3");
        }

       

        private void button4_Click_1(object sender, EventArgs e)
        {
            try
            {
                string buzzerType = comboBox2.SelectedItem.ToString();
                switch (buzzerType)
                {
                    case "Simple":
                        consoleCommand("http://" + consoleIp + "/buzzer.ps3mapi?mode=1");
                        break;
                    case "Double":
                        consoleCommand("http://" + consoleIp + "/buzzer.ps3mapi?mode=2");
                        break;
                    case "Triple":
                        consoleCommand("http://" + consoleIp + "/buzzer.ps3mapi?mode=3");
                        break;

                }
            }
            catch (Exception err)
            {
                MessageBox.Show("You need to select an item.");
            }

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click_2(object sender, EventArgs e)
        {
            Clipboard.SetText(label4.Text + "\n" + label5.Text);
            MessageBox.Show("Your PSID and IDPS have copied to the clipboard");
        }
    }
}
